---
date: 2020-03-14T10:44:38.876Z
endDate: 2020-06-01T10:44:38.876Z
title: Wessel Kuiper
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: WES4.jpg
gallery:
  - image: WES4.jpg
  - image: WES5.jpg
  - image: WES8.jpg
---
Wessel schildert veelal stadsgezichten. In het lijnenspel en de vlakverdelingen van gevels en daken zoekt hij een boeiende compositie die hij, lettend op perspectief, op vrij realistische wijze en sfeervol op doek probeert te zetten. Het spelen met lijnen, horizontalen verticalen en diagonalen, is een belangrijk element in zijn werk

Deze expositie valt gedeeltelijk samen met KinderKunstWeek, die loopt van 11 t/m 22 maart. In die periode is de galerie dagelijks geopend vanaf 13:00 uur. Op afspraak is het ook mogelijk in de morgen een bezoek te brengen. Het thema van dit jaar is: “Het huis in de kunst”. Dit thema sluit goed aan bij het werk van Wessel Kuiper. Verder is er een model-huis te zien in Bauhaus stijl, met mini-Mondriaantjes en mini-Rietveld meubeltjes. Kinderen kunnen ook meedoen aan een kleine speurtocht in de directe omgeving: “herken je dit huis”. Voor goede oplossers is er een prijsje beschikbaar in de vorm van een klein zeefdrukje.
