---
date: 2023-04-29T17:44:01.002Z
endDate: 2023-06-04T17:44:01.013Z
title: Karolien Boucquaert
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: dsc_7302a.jpg
gallery:
  - image: dsc_7331.jpg
  - image: 00000006.jpg
  - image: dsc_7520x.png
---
Karolien Boucquaert studeerde zeefdruk, keramiek, en edelsmeedkunst in Eeklo en St. Niklaas. Ze heeft deelgenomen aan vele exposities, in België, Frankrijk, Nederland, Duitsland en Griekenland. In haar atelier worden regelmatig diverse soorten zeefdruk-workshops gegeven.

Karolien woont en werkt in Lembeke, België, niet ver van Gent. Ze is sinds jaren gefascineerd door zeefdruk, en experimenteert met verschillende technieken, inkten en materialen zoals papier, schilderdoek, karton, plexi, keramiek, porselein en glas. Het resultaat is steeds artistiek, kleurrijk en uniek.
Ze behaalde diverse selecties en prijzen, in 1999 werd ze laureaat  van de stad Gent in de wedstrijd ‘Kunst in de Buurt’.  Haar werk was de laatste jaren ook te zien in volgende musea: DIVA Antwerpen, Hof Van Busleyden Mechelen, Paper Art 2019, Challenge Accepted 2020, CODA Apeldoorn.

De expositie in Galerie Luctor van zeefdrukken en sieraden start op 29 april, als onderdeel van de kunstroute “Kunstspeuren in de Zak”. Op 4 juni wordt de expositie afgesloten met een “finissage”.

https://karolienboucquaert.wixsite.com/karolien-boucquaert