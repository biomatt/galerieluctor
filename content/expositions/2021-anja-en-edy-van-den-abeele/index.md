---
date: 2021-05-01T18:49:30.678Z
endDate: 2021-06-06T18:49:30.720Z
title: Anja en Edy van den Abeele
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: vis.jpg
---
Edy van den Abeele maakt houtsculpturen en acrylschilderijen. Anja van den Abeele maakt beelden van keramiek. Beiden wonen en werken in Westdorpe (Zeeuwsvlaanderen).

De creaties van Anja beelden vaak pittige mollige dames uit.. Ze zoekt het vooral in beelden met humor, die je eigen fantasie kunnen laten spreken. Anja noemt zich een mensenmens, en van daaruit haalt ze haar inspiratie. Ze gebruikt veelal chamotte klei en maakt haar glazuren zelf. Anja heeft een keramiek studie gevolgd aan de kunstacademie van Eeklo.

Edy is als autodidact in 1990 begonnen met acryl schilderijen. In zijn veelal abstracte schilderijen gebruikt hij diverse technieken. Het is voor hem elke keer weer spannend om te zien hoe hij met treffende kleuren en vormen tot een mooi resultaat kan komen.

In de laatste jaren heeft hij zich ook toegelegd op houtsculpturen als expressievorm. Hij begint met een blok hout, en verlijmt dat met lagen multiplex en MDF. Dan gaat hij aan het werk om er vormen met vloeiende lijnen uit te krijgen. Het resultaat is veelal van een verbluffende schoonheid.

Een recente nieuwe richting is het maken van tassen van hout, werkelijk heel bijzonder.

Meer informatie over beide kunstenaars: www.abeelekunst.nl
