---
date: 2022-11-26T08:36:18.997Z
endDate: 2023-01-07T08:36:19.004Z
title: Zeefdrukken uit de Workshop Zeefdrukken
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: workshop.jpg
gallery:
  - image: carolien.png
  - image: marian.png
  - image: bram.png
  - image: dennis.png
  - image: tineke.png
  - image: marian-2-.png
---
Werken van Bram Sturm, Tineke van der Ven, Marian Fackler, Caroline de Jonge, Dennis Menheere

