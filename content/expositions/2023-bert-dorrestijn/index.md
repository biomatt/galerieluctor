---
date: 2023-09-02T08:40:44.546Z
endDate: 2023-10-08T08:40:44.557Z
title: Bert Dorrestijn
planned: false
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: featured_image.jpg
gallery:
  - image: 20220723_081404.jpg
  
 
---
In september exposeert Bert Dorrestijn met schilderijen en beelden. Bert is een echt natuurmens. Hij is geboren in Bussum, vlak bij de heidevelden. Vaak was hij daar aan het struinen. Dat gaf hem emotionele rust. De Eempolder, Oostvaarders plassen, ’s Gravelandse buitengebieden waren plekken waar hij in zijn jeugd veel vertoefde. Van jongs af aan tekende en schilderde hij ook veel. Na de HAVO wilde hij eigenlijk boswachter worden. Uiteindelijk werd het toch een creatieve richting: de Kunst Academie te Utrecht waar hij de opleiding ‘Vrije Grafiek’ volgde.

Natuur en cultuur vormen de rode draad in al zijn werk. Van exotische portretten tot Zeeuwse en Brabantse landschappen vormen het merendeel van zijn onderwerpen. Hoewel opgeleid als graficus valt zijn werk als impressionistisch te karakteriseren. Sinds lange tijd woont en werkt Bert in Vlissingen. Hij houdt van die stad, met zijn rauwheid, puurheid en de zee. Hij verwondert zich over het universum, moeder aarde, plant, dier en mens, en al wat er is. “Laten wij zuinig zijn op dat wat ons innerlijke rust geeft in een woelige wereld.” 
