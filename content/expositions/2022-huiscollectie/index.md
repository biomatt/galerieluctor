---
date: 2023-01-08T10:30:25.134Z
endDate: 2023-04-26T22:00:00.000Z
title: Huiscollectie
planned: true
publishDate: 2020-01-01T00:00:00.000Z
resources:
  - name: featured_image
    src: decock.jpg
---
In de wintermaanden (oktober - april) is de galerie (meestal) open op de gebruikelijke tijden. We tonen dan kunstwerken uit onze collectie, en eigen zeefdrukken.
